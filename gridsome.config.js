const sourceFilesystem = require('./plugins/plugin.sourceFilesystem');

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Veille OCoPP',
  siteDescription:
    "Plateforme de veille et de suivi des distributions",

  plugins: [sourceFilesystem],
  templates: {},
};
