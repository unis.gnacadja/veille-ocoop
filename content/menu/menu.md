# Main header

## Services

- Compte de paiement
  - /compte-paiement/
- Compte bloqué
  - /compte-bloque/
- Logement étudiant
  - /logement-etudiant/
- Assurances
  - /assurance/
## Tarifs

- France
  - /tarifs?country=france
- Allemagne
  - /tarifs?country=allemagne
  
- Offres promotionnelles <span class="has-text-danger">*</span>
  - /promo/
- Parrainage 2022
  - /parrainage2022
## Studely

- Découvrir Studely
  - /decouvrir-studely/
- Bourse
  - /bourse/
- Club Studely
  - /club-studely/
- Partenariat
  - /partenariat/

- Nous contacter
  - /contactez-nous/

- Nous rejoindre
  - /nous-rejoindre/

## Contrôle compte bloqué

- /controle/
