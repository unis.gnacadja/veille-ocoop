# Main header

## Services

- Payment account
  - /compte-paiement/
- Blocked account
  - /compte-bloque/
- Student Housing
  - /logement-etudiant/
- Travel and/or home insurance
  - /assurance/

## Pricing

- France
  - /tarifs?country=france
- Germany
  - /tarifs?country=allemagne
- Promotional offers <span class="has-text-danger">*</span>
  - /promo/
- Sponsorship 2022
  - /parrainage2022
## Studely

- Discover Studely
  - /decouvrir-studely/
- Scholarship
  - /bourse/
- Studely club
  - /club-studely/
- Partnership
  - /partenariat/

- Contact us
  - /contactez-nous/

- Join us
  - /nous-rejoindre/


## Blocked account control

- /controle/
