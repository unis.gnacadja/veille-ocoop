---
title: Présentation des indicateurs de performances de la collecte de données au Cameroun
published: true
date: 2022-11-15
---

## Liste des indicateurs du Cameroun

**1- Nombre de centres**, indique le nombre de centres de santé et de pharmacies inclus dans le programme.  


**2- Taux de centres ayant un protocole de stockage adéquat**, présente le pourcentage de centres de santé et de pharmacies utilisant un protocole de stockage adéquat conforme aux normes défini par Positive Generation.  
  

**3- Taux d'accès aux médicaments des personnes vulnérables**, présente le pourcentage de centres de santé et pharmacies distribuant des médicaments aux personnes vulnérables dans le cadre du programme pendant une période P.  


**4- Produits les plus souvent en rupture de stock**,présente les 10 produits les plus souvent en rupture de stock pendant une période P.  


**5- Centres les plus souvent en ruptures**, présente les 10 centres de santés et pharmacies les plus souvent en rupture de stock pendant une période P.  


**6- Taux de centres ayant connu une rupture de stock**, présente le pourcentage de centres de santés et de pharmacies ayant connu une rupture de stock sur l’un des produits distribués pendant une période P.  


**7- Taux de satisfaction d’approvisionnement**, présente l'évolution mensuelle du pourcentage de centres de santés et de pharmacies ayant réceptionnées des produits conforme aux demandes effectuées dans le cadre du programme pendant une période P.  


**8- Taux de centre dont le stock réceptionné est en mauvais état**, présente l'évolution mensuelle du pourcentage de centres de santés et de pharmacies ayant réceptionnées des produits en mauvais état dans le cadre du programme pendant une période P.  


**9- Durée moyenne de rupture de stock**, indique le nombre de jours en moyenne d'indisponibilité de produits pendant une période P.  

