/* eslint-disable func-names */
/* eslint-disable import/no-unresolved */
// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import Buefy from 'buefy';
import '~/main.scss';
import Axios from 'axios';
import DefaultLayout from '~/layouts/Default.vue';

export default function (Vue, { head }) {
  // Add attributes to HTML tag
  // eslint-disable-next-line no-param-reassign
  head.htmlAttrs = { lang: 'fr' };

  // import vue meta
  head.meta.push({
    name: 'viewport',
    content: 'width=device-width, initial-scale=1.0, shrink-to-fit=no',
  });

  // Import Font Awesome
  head.link.push({
    rel: 'stylesheet',
    href: 'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
  });

  head.link.push({
    rel: 'preconnect',
    href: 'https://fonts.gstatic.com',
  });

  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css2?family=Poppins:wght@400&display=swap',
  });

  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700;800&display=swap', // secondary text
  });

  // Set default layout as a global component
  Vue.component('LayoutVue', DefaultLayout);
  // Register our Bulma component library
  Vue.use(Buefy);
  // http requests via axios
  Vue.use(Axios);

  // Body ELASTICSEARCH
  Vue.prototype.$elasticSearch = {
    cameroun: {
      aggs: {
        center_count: {
          cardinality: {
            field: 'name.keyword',
          },
        },
        taux_approvisionnement: {
          filter: {
            bool: {
              filter: [
                {
                  bool: {
                    should: [
                      {
                        match: {
                          'stock.is_ask_stock': true,
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        },
        average_out_of_stock_duration: {
          avg: {
            field: 'stock.out_of_stocks.duration',
          },
        },
        taux_peremption: {
          filter: {
            bool: {
              filter: [
                {
                  bool: {
                    should: [
                      {
                        match: {
                          'stock.is_stock_spoiled': true,
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        },
      },
      size: 0,
    },
    burkina: {
      aggs: {
        centers: {
          cardinality: {
            field: 'name.keyword',
          },
        },
        out_stocks_times: {
          sum: {
            field: 'products.out_stocks_times',
          },
        },
        ruptures: {
          sum: {
            field: 'products.nb_rupture',
          },
        },
        received: {
          sum: {
            field: 'products.received',
          },
        },
        asked: {
          sum: {
            field: 'products.asked',
          },
        },
        expired: {
          sum: {
            field: 'products.expired',
          },
        },
        spoiled: {
          sum: {
            field: 'products.spoiled',
          },
        },
        init: {
          sum: {
            field: 'products.init_stock',
          },
        },
      },
      size: 0,
    },
  };
}
